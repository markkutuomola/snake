import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;

public class AloitusController {
    //togglegroup vaikeustasolle
    static ToggleGroup difficulty = new ToggleGroup();
    //togglegroup kontrollien valintaan
    static ToggleGroup controls = new ToggleGroup();

    /**
     * Initialisoidaan togglegroupit
     * Oletuksena valittuna nopeus 1 ja ohjaimet wasd
     */
    public void initialize() {
        n1.setToggleGroup(difficulty);
        n2.setToggleGroup(difficulty);
        n3.setToggleGroup(difficulty);
        n4.setToggleGroup(difficulty);
        n5.setToggleGroup(difficulty);
        n6.setToggleGroup(difficulty);
        n7.setToggleGroup(difficulty);
        n8.setToggleGroup(difficulty);

        n1.setSelected(true);

        wasd.setToggleGroup(controls);
        nuolet.setToggleGroup(controls);

        wasd.setSelected(true);
    }

    /**
     * Metodi vaikeustason valintaan
     * @return selection.getText
     */
    public static String difficultySelected(){
        RadioButton selection= (RadioButton) difficulty.getSelectedToggle();
        return selection.getText();
    }

    /**
     * Metodi ohjainten valintaan, mikäli valittuna WASD
     * @return true
     * jos valinta on Nuolet
     * @return false
     */
    public static boolean controlSelected() {
        RadioButton selectedControls =(RadioButton)controls.getSelectedToggle();
        if (selectedControls.getText().equals("WASD")) {
            return true;
        }
        else {
            return false;
        }
    }


    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Label ohjain;

    @FXML
    private RadioButton wasd;

    @FXML
    private RadioButton nuolet;

    @FXML
    private Label nopeus;

    @FXML
    private RadioButton n1;

    @FXML
    private RadioButton n3;

    @FXML
    private RadioButton n2;

    @FXML
    private RadioButton n4;

    @FXML
    private RadioButton n5;

    @FXML
    private RadioButton n6;

    @FXML
    private RadioButton n7;

    @FXML
    private Button aloita;

    @FXML
    private RadioButton n8;

    /**
     * Aloitus napin painamiselle
     * @param event
     * Ohjelman tilaksi asetetaan peli
     */
    @FXML
    void aloitaAction(ActionEvent event) {
        Main.setAppState(Main.AppState.peli);
    }
}

