import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;

import javafx.scene.control.Button;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.StrokeType;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.io.IOException;

public class Main extends Application {

    public enum AppState {valikko, peli}
    private static ObjectProperty<AppState> tila = new SimpleObjectProperty<>();

    public static void setAppState(AppState uusiTila){tila.set(uusiTila);}

    public static final int game_width = 600;
    public static final int game_height = 600;
    public static final int block_Size = 20;

    private Rectangle food;
    private Rectangle bodyPiece;
    private ObservableList<Node> mato;

    private Timeline time;

    private double gameSpeed;

    private boolean gameOn = true;

    private enum Direction {
        UP, DOWN, LEFT, RIGHT
    }
    //initial direction
    private Direction direction = Direction.RIGHT;

    private boolean pressed;

    /**
     * Metodi pelin aloitukselle.
     * @param  primaryStage
     */
    @Override
    public void start(Stage primaryStage) throws Exception {

        tila.addListener((observable, oldValue, newValue)->
                {
                    if (newValue == AppState.peli){
                        gameSpeed = 1-((Double.parseDouble(AloitusController.difficultySelected())/10));
                        System.out.println(gameSpeed);

                        Scene game = new Scene(gameLogic());
                        primaryStage.setScene(game);
                        startGame();

                        //madon ohjaaminen nuolinäppäimillä
                        game.setOnKeyPressed(event -> {
                            if (pressed && !AloitusController.controlSelected()) {
                                switch (event.getCode()) {
                                    case UP:
                                        if (direction != Direction.DOWN)
                                            direction = Direction.UP;
                                        break;
                                    case DOWN:
                                        if (direction != Direction.UP)
                                            direction = Direction.DOWN;
                                        break;
                                    case LEFT:
                                        if (direction != Direction.RIGHT)
                                            direction = Direction.LEFT;
                                        break;
                                    case RIGHT:
                                        if (direction != Direction.LEFT)
                                            direction = Direction.RIGHT;
                                        break;
                                }
                                //madon ohjaaminen WASDilla
                            } else if (pressed && AloitusController.controlSelected()){
                                switch (event.getCode()) {
                                    case W:
                                        if (direction != Direction.DOWN)
                                            direction = Direction.UP;
                                        break;
                                    case S:
                                        if (direction != Direction.UP)
                                            direction = Direction.DOWN;
                                        break;
                                    case A:
                                        if (direction != Direction.RIGHT)
                                            direction = Direction.LEFT;
                                        break;
                                    case D:
                                        if (direction != Direction.LEFT)
                                            direction = Direction.RIGHT;
                                        break;
                                }
                            }
                            else {
                                event.consume();
                            }
                            pressed=false;
                        });
                    }
                    else {
                        try {
                            primaryStage.setScene(new Scene(FXMLLoader.load(getClass().getResource("Aloitus.fxml")), 600, 238));

                        } catch (IOException e){
                            e.printStackTrace();
                        }
                    }
                }
        );

        primaryStage.setTitle("Matopeli");
        primaryStage.setScene(new Scene(FXMLLoader.load(getClass().getResource("Aloitus.fxml")), 600, 238));
        primaryStage.show();

    }
    private Parent gameLogic() {
        Pane root = new Pane();
        root.setPrefSize(game_width, game_height);

        //luodaan ruoka
        food = new Rectangle(block_Size, block_Size);
        food.setFill(Color.ORANGE);
        food.setStroke(Color.ORANGERED);
        food.setStrokeType(StrokeType.INSIDE);
        food.setStrokeWidth(2);

        food.setLayoutX((int) (Math.random() * game_width)/block_Size *block_Size);
        food.setLayoutY((int) (Math.random() * game_height)/block_Size *block_Size);

        //luodaan madon vartalo
        Group bodylist = new Group();
        mato = bodylist.getChildren();

        KeyFrame frame = new KeyFrame(Duration.seconds(gameSpeed), evt -> {
            if(!gameOn) {
                return;
            }
            bodyPiece = new Rectangle(block_Size, block_Size);
            bodyPiece.setFill(Color.LIGHTGREEN);
            bodyPiece.setStroke(Color.GREEN);
            bodyPiece.setStrokeType(StrokeType.INSIDE);
            bodyPiece.setStrokeWidth(2);

            bodyPiece.setLayoutX(mato.get(0).getLayoutX());
            bodyPiece.setLayoutY(mato.get(0).getLayoutY());

            mato.add(0,bodyPiece);
            mato.remove(mato.size()-1);

            //madon liikkuminen eri suunnissa
            switch (direction) {

                case UP:
                    mato.get(0).setLayoutX(mato.get(0).getLayoutX());
                    mato.get(0).setLayoutY(mato.get(0).getLayoutY() - block_Size);
                    break;
                case RIGHT:
                    mato.get(0).setLayoutX(mato.get(0).getLayoutX() + block_Size);
                    mato.get(0).setLayoutY(mato.get(0).getLayoutY());
                    break;

                case DOWN:
                    mato.get(0).setLayoutX(mato.get(0).getLayoutX());
                    mato.get(0).setLayoutY(mato.get(0).getLayoutY() + block_Size);
                    break;

                case LEFT:
                    mato.get(0).setLayoutX(mato.get(0).getLayoutX() - block_Size);
                    mato.get(0).setLayoutY(mato.get(0).getLayoutY());
                    break;
            }
            pressed = true;

            //ruuan syöminen ja "uuden" generointi
            if (mato.get(0).getLayoutX()==food.getLayoutX() && mato.get(0).getLayoutY()==food.getLayoutY()){
                int foodX, foodY;
                foodX= (int) (Math.random() * game_width)/block_Size *block_Size;
                foodY= (int) (Math.random() * game_height)/block_Size *block_Size;

                boolean overlap=false;

                for (Node bodyPiece : mato) {
                    if(bodyPiece.getLayoutX()==foodX && bodyPiece.getLayoutY()==foodY){
                        overlap=true;
                    }
                }
                if (!overlap) {
                    food.setLayoutX(foodX);
                    food.setLayoutY(foodY);
                    mato.add(mato.size(),new Rectangle());
                }
                else {
                    System.out.println("Maha täynnä");
                    foodX= (int) (Math.random() * game_width)/block_Size *block_Size;
                    foodY= (int) (Math.random() * game_height)/block_Size *block_Size;
                    food.setLayoutX(foodX);
                    food.setLayoutY(foodY);
                }
            }

            //reunoihin törmäys
            if (mato.get(0).getLayoutX() >= game_width || mato.get(0).getLayoutX() <0 || mato.get(0).getLayoutY() >= game_height || mato.get(0).getLayoutY() < 0) {
                Stage dialog = new Stage();
                dialog.initModality(Modality.APPLICATION_MODAL);

                VBox dialogVbox = new VBox(20);
                Button button1 = new Button("Restart");
                Button button2 = new Button("Alkuvalikko");

                button1.setOnAction(e ->{

                    restart();
                    dialog.close();
                });

                button2.setOnAction(e ->{
                    setAppState(AppState.valikko);
                    dialog.close();

                });
                dialogVbox.getChildren().addAll(new Text("Kuolit!"), button1, button2);
                dialogVbox.setAlignment(Pos.CENTER);
                Scene dialogScene = new Scene(dialogVbox, 150, 150);
                dialog.setScene(dialogScene);
                dialog.show();

                time.stop();
            }
            //self collision
            for(Node pala : mato) {
                //pakko määrittää pala != mato.get(0), muuten peli restarttii aina syömisen jälkeen
                if (pala != mato.get(0) && mato.get(0).getLayoutX() == pala.getLayoutX() && mato.get(0).getLayoutY() == pala.getLayoutY()) {
                    restart();
                    break;
                }
            }
        }
        );

        time = new Timeline();
        time.getKeyFrames().add(frame);
        time.setCycleCount(Timeline.INDEFINITE);

        root.getChildren().addAll(food, bodylist);

        return root;
    }
    /**
     * Metodi pelin restart:lle.
     * Metodia kutsutaan, kun mato törmää itseensä ja peli aloitetaan uudestaan.
     * Metodia kutsutaan myös, kun mato törmää seinään ja käyttäjä valitsee avautuneesta ikkunasta
     * vaihtoehdon "restart".
     */
    private void restart() {
        stopGame();
        startGame();
    }

    /**
     * Metodi pelin aloitukselle. Metodissa alustetaan mato, asetetaan väri, mato asetetaan koordinaattiin 0,0 ja
     * liikkumissuunnaksi lähtöpisteestä oikealle.
     */

    private void startGame(){
        Rectangle startMato = new Rectangle(block_Size, block_Size);
        startMato.setFill(Color.LIGHTGREEN);
        startMato.setStroke(Color.GREEN);
        startMato.setStrokeType(StrokeType.INSIDE);
        startMato.setStrokeWidth(2);

        mato.add(startMato);
        direction = Direction.RIGHT;
        mato.get(0).setLayoutX(0);
        mato.get(0).setLayoutY(0);
        gameOn = true;
        time.play();
    }

    /**
     * Metodi pelin pysäyttämiselle. Pelitilanne nollataan.
     */
    private void stopGame(){
        gameOn = false;
        time.stop();
        mato.clear();
    }
    public static void main(String[] args) {
        launch(args);
    }
}
